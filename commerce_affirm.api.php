<?php

/**
 * @file
 * Hooks provided by the Commerce Affirm module.
 */

/**
 * Allows modules to alter the affirm js checkout object.
 *
 * @param $data
 *   The array that will turn into the js checkout object. See
 *   https://docs.affirm.com/Integrate_Affirm/Direct_API#Checkout_object
 * @param $order
 *   The order entity.
 * @param $settings
 *   The payment method instance settings.
 */
function hook_commerce_affirm_transaction_data_alter(&$data, $order, $settings) {
  foreach ($data['items'] as &$item) {
    if (strpos($item->sku, 'HANDBAG') !== FALSE) {
      $item['categories'][] = ['Swag', 'Bag', 'Handbag'];
    }
  }
}
