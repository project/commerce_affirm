<?php

/**
 * @file
 * Ensures users have cURL enabled prior to installation.
 */

/**
 * Implements hook_requirements().
 */
function commerce_affirm_requirements($phase) {
  // Skip the requirements check if SimpleTest is installed to avoid multiple
  // cURL rows.
  if (module_exists('simpletest')) {
    return;
  }

  $t = get_t();

  $has_curl = function_exists('curl_init');

  $requirements['commerce_affirm'] = array(
    'title' => $t('cURL'),
    'value' => $has_curl ? $t('Enabled') : $t('Not found'),
  );

  if (!$has_curl) {
    $requirements['commerce_affirm'] += array(
      'severity' => REQUIREMENT_ERROR,
      'description' => $t("Affirm requires the PHP <a href='!curl_url'>cURL</a> library.", array('!curl_url' => 'http://php.net/manual/en/curl.setup.php')),
    );
  }

  return $requirements;
}

/**
 * Implements hook_uninstall().
 */
function commerce_affirm_uninstall() {
  variable_del('commerce_affirm_analytics');
  variable_del('commerce_affirm_monthly_payment_on_add_to_cart');
}

/**
 * Create live and sandbox API keys.
 */
function commerce_affirm_update_7001() {
  $payment_method_instance = _commerce_affirm_get_affirm_payment_method_instance();
  /** @var $rule RulesReactionRule */
  $rule = entity_load_single('rules_config', $payment_method_instance['rule_name']);
  if ($rule instanceof RulesReactionRule) {
    foreach ($rule->getIterator() as $child) {
      /** @var $child RulesAction */
      if ($child->property('elementName') === 'commerce_payment_enable_affirm') {
        $api_keys = array('public_key', 'private_key', 'financial_key');
        foreach ($api_keys as $api_key) {
          if (isset($child->settings['payment_method']['settings'][$api_key])) {
            $child->settings['payment_method']['settings']['live'][$api_key] = $child->settings['payment_method']['settings'][$api_key];
            $child->settings['payment_method']['settings']['sandbox'][$api_key] = $child->settings['payment_method']['settings'][$api_key];
            unset($child->settings['payment_method']['settings'][$api_key]);
          }
        }
      }
    }
    entity_save('rules_config', $rule);
  }
}
