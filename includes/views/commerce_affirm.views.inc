<?php

/**
 * Provide Commerce Affirm related Views integration.
 */

/**
 * Implements hook_views_data().
 */
function commerce_affirm_views_data_alter(&$data) {
  $data['commerce_product']['affirm_monthly_payment_message'] = array(
    'field' => array(
      'title' => t('Affirm promotional messaging'),
      'help' => t('Display Affirm\'s monthly payment message for a product.'),
      'handler' => 'commerce_affirm_handler_field_monthly_payment_message_product',
    ),
  );

  $data['commerce_order']['affirm_monthly_payment_message_order'] = array(
    'title' => t('Affirm promotional messaging'),
    'help' => t('Display Affirm\'s monthly payment message for an order.'),
    'area' => array(
      'handler' => 'commerce_affirm_handler_field_monthly_payment_message_order',
    ),
  );
}
