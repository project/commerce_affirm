/**
 * @file
 * Enhanced Affirm Analytics.
 */

(function () {
  Drupal.behaviors.commerce_affirm_checkout_complete = {
    attach: function (context, settings) {
      affirm.ui.ready(function() {
        affirm.analytics.trackOrderConfirmed(settings.commerce_affirm.checkoutComplete.order, settings.commerce_affirm.checkoutComplete.products);
      });
    }
  }
})();
