<?php

/**
 * @file
 * Default theme implementation to present the Affirm monthly payment message.
 *
 * Available variables:
 * - $amount: The price amount.
 * - $type: The data-page-type attribute of the affirm html element.
 * - $product: The commerce_product entity.
 */
?>
<?php drupal_add_js(drupal_get_path('module', 'commerce_affirm') . '/js/commerce_affirm.js', array('weight' => -10)); ?>
<div class="commerce-product-affirm-monthly-payment-message">
  <p class="affirm-as-low-as" data-page-type="<?php print $type; ?>" <?php if (!empty($product)): print ' data-sku="' . $product->sku . '" '; endif; ?> data-amount="<?php print $amount; ?>"></p>
</div>
